﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Space_Cat : MonoBehaviour {

    Rigidbody rigidBody;
    AudioSource audioSource;

    [SerializeField] ParticleSystem mainEngineParticles ;
    [SerializeField] ParticleSystem successParticles;
    [SerializeField] ParticleSystem deathParticles;

    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip deathExplosion;
    [SerializeField] AudioClip winSound;

    [SerializeField ]float rcsThrust = 100f;
    [SerializeField] float mainThrust = 50f;
    [SerializeField] float levelLoadDelay = 2f;

    bool isTransitioning = false;

    bool collisionsAreDisabled = true;
    

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!isTransitioning)
        {
            RespondToThrustInput();
            ResponToRotateInput();
        }

        // todo only if debug on
        if (Debug.isDebugBuild)
        {
            RespondToDebugKeys();
        }
    
       
    }

    private void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextScene();
        }

        else if (Input.GetKeyDown(KeyCode.C))
        {
            collisionsAreDisabled = !collisionsAreDisabled; //toggle
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if ( isTransitioning || !collisionsAreDisabled) { return; } //ignores collision when dead

        switch (collision.gameObject.tag)
        {
            case "Friendly": 
                // do nothing

                break;

            case "Finnish":
                StartSuccessSequence();
                break;

            default:
                StartDeathSequence();
                break;
        }
    }

    private void StartSuccessSequence()
    {
        isTransitioning = true;
        Invoke("LoadNextScene", levelLoadDelay);
        audioSource.Stop();
        audioSource.PlayOneShot(winSound);
        successParticles.Play();
    }
    private void StartDeathSequence()
    {
        isTransitioning = true;
        Invoke("LoadFirstScene", levelLoadDelay);
        audioSource.Stop();
        audioSource.PlayOneShot(deathExplosion);
        deathParticles.Play();
    }


    private void LoadFirstScene()
    {
        SceneManager.LoadScene(0);

    }

    private void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if ( nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0; // loop back to start
        }
        SceneManager.LoadScene(nextSceneIndex); // todo allow more than 2 levels
    }

    private void ResponToRotateInput()
    {
        rigidBody.angularVelocity = Vector3.zero; // remove rotation due to physics

        float rotationThisFrame = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotationThisFrame);
        }

        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward * rotationThisFrame);
        }

    }

    private void RespondToThrustInput()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();
        }
        else
        {
            StopApplyingThrust();
        }
    }

    private void StopApplyingThrust()
    {
        audioSource.Stop();
        mainEngineParticles.Stop();
    }

    private void ApplyThrust()
    {
        rigidBody.AddRelativeForce(Vector3.up * mainThrust);

        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);
        }
        mainEngineParticles.Play();
    }
}
